<?php
	/**
	 * Created by PhpStorm.
	 * User: ilasmahianov
	 * Date: 17/02/2019
	 * Time: 14:56
	 */
	
	namespace CloudMind\BlankSite;
	
	use Symfony\Component\DependencyInjection\ContainerBuilder;
	use Symfony\Component\HttpKernel\Bundle\Bundle;
	
	class CloudMindBlankSite extends Bundle
	{
		protected $env;
		
		public function build(ContainerBuilder $container)
		{
			parent::build($container);
		}
	}